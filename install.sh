#!/bin/sh

# Find this script, and work from there. The dotfiles can be put in any directory, they'll find their way to the right place.
working_dir=$(dirname "$0")   # location of this script
cd "$working_dir" || exit

olddir=$HOME/.dotfiles_old   # Backups of previous dot files
dots=$(pwd)/dots     # Dotfiles that go into the homedir
config=$(pwd)/config # Dotfiles that go into the .config dir

printf "Creating %s and necessary subfolders for backup of existing dotfiles\\n" "$olddir"
mkdir -p "$olddir/.config"
mkdir -p "$HOME/.config"
printf "...done\\n\\n"

if [ ! "$1" = "-f" ]; then
	if [ -d "$olddir" ]; then
		printf "Warning, backup directory exists. It's contents will be overwritten. Continue? [y/n] "
		read -r ans
		if [ "$ans" = "n" ]; then
			exit
		fi
		if [ "$ans" != "y" ]; then
			printf "Invalid response. Please rerun the script and confirm that you understand that the old backup directory will be overwritten.\\n"
			exit
		fi
	fi
fi

# Backup the files, and symlink to homedir
for file in "$dots"/*; do
	f="$HOME/.$(basename "$file")"
	if [ -f "$f" ] || [ -d "$f" ] || [ -L "$f" ]; then
	#if [ -f "$f" ] || [ -d "$f" ] || {[ -L "$f" ] && [ ! -e "$f" ]}; then
		printf "Backing up %s to %s\\n" "$f" "$olddir"
		mv -f "$f" "$olddir"
	fi
	printf "Symlinking %s to %s\\n" "$file" "$f"
	ln -s "$file" "$f"
done

printf "\\n\\n"

# Repeat for .config files
for file in  "$config"/*; do
	f="$HOME/.config/$(basename "$file")"
	if [ -f "$f" ] || [ -d "$f" ] || [ -L "$f" ]; then
		printf "Backing up %s to %s/.config\\n" "$f" "$olddir"
		mv -f "$f" "$olddir"/.config
	fi
	printf "Symlinking %s to %s\\n" "$file" "$f"
	ln -s "$file" "$(dirname "$f")"
done

if [ ! -d "$HOME/.oh-my-zsh/" ]; then
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" "" --unattended
	ln -srf "themes/customflazz.zsh-theme" "$HOME/.oh-my-zsh/themes"
	ln -sf "$dots/zshrc" "$HOME/.zshrc"
fi
if [ ! -d "$HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting" ]; then
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "${ZSH_CUSTOM:-"$HOME/.oh-my-zsh/custom"}/plugins/zsh-syntax-highlighting"
fi
if [ ! -d "$HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions" ]; then
	git clone https://github.com/zsh-users/zsh-autosuggestions "${ZSH_CUSTOM:-"$HOME/.oh-my-zsh/custom"}/plugins/zsh-autosuggestions"
fi
if [ ! -d "$HOME/.oh-my-zsh/custom/plugins/careful_rm" ]; then
	git clone https://github.com/MikeDacre/careful_rm "${ZSH_CUSTOM:-"$HOME/.oh-my-zsh/custom"}/plugins/careful_rm"
fi
if [ ! -d "$HOME/.vim/bundle/vim-airline" ]; then
	git clone https://github.com/vim-airline/vim-airline.git "$HOME/.vim/bundle/vim-airline"
	cp themes/x.vim "$HOME/.vim/bundle/vim-airline/autoload/airline/themes/"
fi

curl -o /tmp/yay.tar.gz https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz
tar -xzvf /tmp/yay.tar.gz
cd yay && makepkg -csi
cd .. && rm -rf yay/

