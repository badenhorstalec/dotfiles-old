#!/bin/sh

# Clear previous rules (no double defining)
bspc rule -r "*"

# Start the needed programs
wmname LG3D
~/dotfiles/scripts/sxhkd.sh
~/dotfiles/scripts/polybar.sh

# Set the number of desktops
if [[ "$HOST" = artix ]]; then
	bspc monitor DVI-D-0 -d 7 8 9 10 11
	bspc monitor HDMI-0 -d 1 2 3 4 5 6
else
	bspc monitor eDP1 -d 1 2 3 4 5 6 7 8 9 10 11
fi

# Window swallowing
prgep bspswallow || bspswallow &

# ??? Internet is never wrong
# Clearly has some effect when I mouse on wallpaper
# Makes it so that your cursor doesn't become "unclickable" when hovering on the wallpaper
xsetroot -cursor_name left_ptr

# Borders and gaps
bspc config border_width         2
bspc config window_gap          12

# Border colours. With compton set to fade unfocused, these aren't so necessary,
# but it looks nice either way
#
# Grab the colours from the Xdefaults file, sort of,
# in a round about way. Maybe there's another way to do this, IDK, but this works
bspc config focused_border_color "$(xrdb -query | sed -n "/alpha/d; s/*.//; /color15/p" | sed "s/color15://; s/\s*//")"
bspc config normal_border_color "$(xrdb -query | sed -n "/alpha/d; s/*.//; /color0/p" | sed "s/color0://; s/\s*//")"
bspc config presel_feedback_color "$(xrdb -query | sed -n "/alpha/d; s/*.//; /color15/p" | sed "s/color15://; s/\s*//")"


# How to split windows
bspc config split_ratio          0.52
# Fullscreen if there is only one window on desktop
bspc config single_monocle       true
bspc config borderless_monocle   true
bspc config gapless_monocle      true

# Mouse control
bspc config pointer_mod mod4
bspc config pointer_action1 move
bspc config pointer_action2 none
bspc config pointer_action3 resize_corner
bspc config click_to_focus true
bspc config focus_follows_pointer true
bspc config swallow_first_click true

# Specific window rules
bspc rule -a Anki desktop='5'
bspc rule -a firefox desktop='4'
bspc rule -a JDownloader desktop='9'
bspc rule -a Discord desktop='7'
bspc rule -a Rambox desktop='10'
bspc rule -a Joplin desktop='9'
bspc rule -a Keepassxc desktop='11'
bspc rule -a Steam desktop='3'
bspc rule -a Zathura state=tiled
bspc rule -a Pcmanfm state=floating
