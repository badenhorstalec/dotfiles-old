To sink or swim, ikinokoru tame ni
Dog-eat-dog world, da to iikikase
What went wrong?
Survival of the fittest, jakuniku kyoushoku
If it’s the world that our wisdom will lead
Well, tell me what went wrong

I’ve paid my dues
I’ve sold my soul
So tell me, what is left for me
When I’ve given up everything?

Nee, dou egaitetan darou?
Dono keshiki wo mitemo
Nanimo kanjinakunatta yo
Nee, kangaenaoshitemo
Ano keshiki wo motometeita
Boku wa mu kanjou na no?

Tsuyoi to iwareru mono ni wa
Sekinin to iu na no kusari wo tsuke
What went wrong?
Yowai to iwarete kita hitotachi wa
Tada fumitsubusarete iku yo no naka
What went wrong?

We've paid our dues
We've sold our souls
So tell me, what is left for us
When we've given up everything?

Nee, dou egaitetan darou?
Dono keshiki wo mitemo
Nanimo kanjinakunatta yo
Nee, kangaenaoshitemo
Ano keshiki wo motometeita
Boku wa mu kanjou na no?

Shinjitsu no mienai sekai
Moraru mo seigi mo kuso kurae da
Tadashii yatsura ga itemo
Tada hitori ga kowashite shimau
Zen to aku no baransu wo tamochi
Korosu ka korosareru ka no
Chippoke na sekai na no ka yo? oi
Dareka kotaete mite kure yo

Nee, dou egaitetan darou?
Dono keshiki wo mitemo
Nanimo kanjinakunatta yo
Nee, kangaenaoshitemo
Ano keshiki wo motometeita
Boku wa mu kanjou na no?

Dee, dou egaitetan darou?
Ano keshiki wo mitemo
Nanimo kanjinakunatta yo
Ano keshiki wo motometeita
Boku wa mu kanjou na no?