Act I - 五頭龍 - Gozuryu

The sea jars is dread
Profane sentence from above
The glance of a shade - shi no kage
Absorbs the vigor of the earth

Watching still with eyes fixed to sky,
Clouds shift away from the serpents tour
Roaming over land like a caliginous storm
The shriek of his ire leaves all ears torn

Knees bent down, ovations, desperate cries
The five archs disdaining the awe
All must perish on his herald of might
The land covered in red, the shores all alike

The souls given for the vile have been remarked
The shadow of the beast moves to south
Heavens turn away for the grand display
Nothing rivals the vastness of this day

Act 2 - The Chord Of A Goddess

Waves break the cliffs and the wind grows ever-strong
The dawn turns to night and in a moment back to light (ten-thousand times)

Celestial yet unnerving scene for everyone
But the call of a sudden faint sound draws our hearts and grit unbreakable

The ring of Benzaiten's note (virtuous)
Save us from damnation's route
When the will of a man is only bound to dissolve
Paramount shall be the ways of the gods

Earth, fire, water in gale
Bow for the goddess and drake
Earth, wind, fire and rain
Bow for the goddess, please banish our bane

Act 3 - Divine Affection

Estranged for decades the beast wept,
For the love for a monster is unrenowned
Deranged by the closing isolation,
Burning hatred grew unmatched
And now accompanied by a heavenly song
He gazes at the goddess in her abode
"Oh fair bringer of everflowing unison
Would your enticing hands ever embrace me"

Act 4 - The Fury Of The Five

You whore! How dare you decline my ardor?
You're nothing for you are one - we are five
You shall burn like those mortals in their worthless heaven
Witness as we rip your throne you foul raven!

Flames reached the firmament
As he charged to slay her holiness
Moments away from a fatal blow
His mind was filled with regret

Act 5 - 龍口山 - Tatsu-no-kuchi Yama

Repentance finally patched his remnants of grace
Thousand years of slaying our kind
She spoke with a voice gentle like a morning dew
Every word a painting - harmony so true

All five bowed down, tears shed to ground
The beast set away facing south
Devotedly watching her world from the shores
Fading to the ground, the mountain of...

Remorse...

The ring of Benzaiten's note (glorious)
Saved us from damnation's route
When the will of a man was bound to dissolve
All witnessed the ways of the gods

Earth, fire, water in gale
Bow for the goddess and drake

Earth, wind, fire and rain
Bow for the goddess, how banish our bane