Turn your head and see the fields of flames

He carries along
From a distant place
He's on his way
He'll bring decay
(Don't move along
'Cause things they will go wrong
The end is getting closer day by day)
In shades of grey
We're doomed to face the night
Light's out of sight

Since we've reached the point of no return
We pray for starlight we wait for the moon
The sky is empty
Alone in the unknown
We're getting nowhere

We have been betrayed
By the wind and rain
The sacred hall's empty and cold
The sacrifice made
Should not be done in vain
Revenge will be taken by Rome

We live a lie
Under the dying moon
Pale-faced laughs doom
Indulges in delight
It's getting out of hand
The final curtain will fall

Hear my voice
There is no choice
There's no way out
You'll find out

We don't regret it
So many men have failed
But now he's gone
Go out and get it
The madman's head it
Shall be thyne
We don't regret it
That someone else dies hidden in disguise
Go out and get it
Orion's hound shines bright

Don't you think it's time to stop the chase
Around the ring
Just stop running, running
Round the ring
Don't you know that fate has been decided by the gods
Feel the distance, distance
Out of reach

Welcome to the end
Watch your step, Cassandra
You may fall
As I've stumbled on the field
Sister mine
(Find myself in darkest places
Find myself drifting away)
Death's a certain thing
And the other world
The other world appears

Find myself she dies in vain

I cannot be freed
I'm falling down
As time runs faster
Moves towards disaster

The ferryman will wait for you
My dear

And then there was silence
Just a voice from other world
Like a leaf in an icy world
Memories will fade

Misty tales and poems lost
All the bliss and beauty will be gone
Will my weary soul find release for a while
At the moment of death I will smile
It's the triumph of shame and disease
In the end Iliad

Raise my hands and praise the day
Break the spell show me the way
In decay
The flame of Troy will shine bright

The newborn child will carry ruin to the hall
The newborn's death would be a blessing to us all

Good choice?
Bad choice?
Out of three
You've chosen misery

Power and wisdom
You deny
Bad choice
(Bad choice)

War is the only answer
When love will conquer fear
So the judgement's been made to the fairest
The graceful says "Badly he fails"

Warning
Fear the heat of passion, Father king
(Don't let him in
Don't let her in)
Desire, lust, obsession
Death they'll bring
(We can't get out
Once they are in)

She's like the sunrise
Outshines the moon at night
Precious like starlight
She will bring in a murderous price

In darkness grows the seed of man's defeat
(Jealousy)
I can clearly see the end now
I can clearly see the end now
I can clearly see the end now

The thread of life is spun
The coin's been placed below my tongue

Never give up
Never give in
Be on our side
So we can win
Never give up
Never give in
Be on our side
Old moon's time is soon to come

Nowhere to run
Nowhere to hide
Nothing to lose
Like one we'll stand
We'll face the storm
Created by a man

Roar roar roar roar
Troy Troy Troy Troy

And as the lion
Slaughters man
I am the wolf
And you're the lamb

Hallowed Troy shall fall
Round the wall
Faith is shattered bodies fall

Nowhere to run
Nowhere to hide
Nothing to lose
Like one we'll stand
It's all for one and one for all
We live for will be wiped out

I feel that something's wrong
(Surprise, surprise they're gone)
Full moon your time goes by
And new moon's still kept out of sight
We live, we die

Misty tales and poems lost
All the bliss and beauty will be gone
Will my weary soul find release for a while
At the moment of death I will smile
It's the triumph of shame and disease
Iliad

Raise my hands and praise the day
Break the spell show me the way
In decay
The flame of Troy will shine bright

Roam in darkness
Spread the vision
We will be lost if you truly believe
Troy in darkness
There's a cold emptiness in our hearts
That they've gone away
And won't come back

They'll tear down the wall to bring it in
They'll truly believe in the lie, lie, lie
With blossoms they'll welcome
The old foe

The vision's so clear
When day and dream unite
The end is near
You better be prepared

The nightmare shall be over now
There's nothing more to fear
Come join in our singing
And dance with us now
The nightmare shall be over now
There's nothing more to fear
The war it is over forevermore

No hope
The blind leads the blind
Carry on
Though future's denied
Mare or stallion
There's far more inside
We are in at the kill we'll cheerfully die

Misty tales and poems lost
All the bliss and beauty will be gone
Will my weary soul find release for a while
At the moment of death I will smile
It's the triumph of shame and disease
Iliad

Raise my hands and praise the day
Break the spell show me the way
In decay
The flame of Troy will shine bright

Holy light shines on

So the judgement's been made
We're condemned though the trial's far ahead
The crack of doom
Father
Your handsome son is heading home

Heading home

Still the wind blows
Calm and silent
Carries news from a distant shore

Out of mind
Can't get it
Can't get it
Out of my head

Sorrow and defeat