I will not play along with things I find unfair
I will not mount defenses against unjust attacks
This land was never given we built this on our own
Insanity dependent whether we rise or fall

Whether we rise or we fall
One for the night, one for the uncontrolled

Hold your feet to the ground
To the end of our time for the rest of our lives
Hold your head on high
To the end of our time for the rest of our lives

Secretly I hope that nothing ever comes of this
And you are not alone wanting to come back
To a place where it won't matter, just what side you're on
It's when our lines are graded, what's underneath will overcome

What's underneath will overcome
One for the night, one for the uncontrolled

Hold your feet to the ground
To the end of our time for the rest of our lives
Hold your head on high
To the end of our time for the rest of our lives

Now it's time to stand up tall
One for the night, one for the uncontrolled

Hold your feet to the ground
To the end of our time for the rest of our lives
Hold your head on high
To the end of our time for the rest of our lives