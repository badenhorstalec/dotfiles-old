# Linux Dotfiles

My personal dotfiles as I've updated them over the past year.

Copy them if you like... or don't, it's up to you.

### Software I use

+ st ([Luke Smiths](https://github.com/lukesmithxyz/st))
+ ranger
+ neovim
+ MPV
+ ncmpcpp + mpd
+ firefox
+ bspwm
+ rofi
+ [tryone144's compton](https://github.com/tryone144/compton)
+ dunst
+ zathura pdf
+ zsh with [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
