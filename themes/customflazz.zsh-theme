if [ "$USER" = "root" ]
then CARETCOLOR="red"
else CARETCOLOR="green"
fi

local return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"
# %m = hostname %n = username
PROMPT='%{${fg[red]}%}[%{${fg[yellow]}%}%n%{${fg[green]}%}@%{${fg[blue]}%}%m%{${fg[red]}%}]%{${fg_bold[magenta]}%} :: %{$reset_color%}%{${fg[white]}%}%c $(git_prompt_info)%{${fg_bold[$CARETCOLOR]}%}➤%{${reset_color}%} '
RPROMPT='%{$fg[$NCOLOR]%} $(vi_mode_prompt_info) ${return_code}%{$reset_color%}'

RPS1='$(vi_mode_prompt_info) ${return_code}'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[cyan]%}‹"
ZSH_THEME_GIT_PROMPT_SUFFIX="› %{$reset_color%}"

MODE_INDICATOR="%{$fg_bold[magenta]%}<%{$reset_color%}%{$fg[magenta]%}<<%{$reset_color%}"

# TODO use 265 colors
MODE_INDICATOR="$FX[bold]$FG[020]<$FX[no_bold]%{$fg[blue]%}<<%{$reset_color%}"
# TODO use two lines if git
