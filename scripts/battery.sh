#!/bin/dash
# Simple script to get battery status on the command line all nicely formatted
# and give a critical level notification at specified levels

export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
export DISPLAY=:0

# Complete status output of the battery
#       state: <state>
#       [time to empty: <time>]
#       percentage: <percentage>
stats=$(upower -i "$(upower -e | grep BAT)" | grep --color=never -E "state|to\ full|to\ empty|percentage" | sed "s/^\s*//")

# Whether bettery is charging
state=$(upower -i "$(upower -e | grep BAT)" | grep --color=never -E "state" | awk '{ print $2 }')

# Percentage of the battery left
percentage=$(upower -i "$(upower -e | grep BAT)" | grep --color=never -E "percentage" | awk '{ print $2 }' | sed "s/%//")

if [ "$state" = "discharging" ]; then
	if [ "$percentage" -lt 51 ] && [ "$percentage" -gt 50 ]; then
		/usr/bin/notify-send -u normal -t 30000 "BATTERY 50%" "$stats"
	fi
	if [ "$percentage" -lt 26 ] && [ "$percentage" -gt 25 ]; then
		/usr/bin/notify-send -u critical -t 60000 "BATTERY 25%" "$stats"
	fi
	if [ "$percentage" -lt 11 ] && [ "$percentage" -gt 9 ]; then
		/usr/bin/notify-send -u critical "BATTERY 10%" "$stats"
	fi
fi
