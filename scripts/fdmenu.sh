#!/bin/bash
# dmenu, but using my theme colours and font
# Needing to change only 1 or 2 instances of dmenu is much more pleasant than changing it for everything that uses dmenu

dmenu -i -fn "PIAPGothic:pixelsize=17" -nb "#282828" -sf "#282828" -sb "#fe9d00" -nf "#fe9d00"

