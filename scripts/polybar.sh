#!/bin/bash
# Launch poly bar, killing any existing instances beofre doing so

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch polybar
for m in $(polybar --list-monitors | cut -d":" -f1); do
	MONITOR=$m polybar --reload -c ~/.config/polybar/config main &
done

# Some confirmation if using script from terminal
echo "Bars launched"
