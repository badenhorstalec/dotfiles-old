#!/bin/sh

if [ "$(pgrep bspwm)" ]; then
	killall polybar
	kill "$(pgrep bspwm)"
	kwin_x11 --replace & disown
else
	kill "$(pgrep kwin)"
	bspwm & disown
fi
