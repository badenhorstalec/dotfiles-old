#!/bin/bash
# Launch SXHKD, killing any currently running instance

# Kill all running instances of SXHKD
killall -q sxhkd

# Wait for the process to shut down
while pgrep -u $UID -x sxhkd >/dev/null; do sleep 1; done

# Relaunch sxhkd
sxhkd &

echo "SXHKD launched"
