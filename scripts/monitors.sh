#!/bin/bash

INTERNAL_DISPLAY="eDP1"
EXTERNAL_DISPLAY="HDMI1"

case "$1" in
	clone)
	# Clone to second monitor
	xrandr --output $INTERNAL_DISPLAY --auto --output $EXTERNAL_DISPLAY --auto --same-as $INTERNAL_DISPLAY
	;;
	extend)
	# Extend to second monitor
	xrandr --output $INTERNAL_DISPLAY --auto --output $EXTERNAL_DISPLAY --auto --right-of $INTERNAL_DISPLAY
	;;
	off)
	# Turn off second monitor
	xrandr --output $INTERNAL_DISPLAY --auto --output $EXTERNAL_DISPLAY --off
	;;
	*)
	echo "invalid command"
	exit2
esac
